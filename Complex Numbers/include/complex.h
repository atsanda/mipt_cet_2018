#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>
#include <cmath>

class Complex {
    private:

    double real;
    double im;
    
    public:

    Complex(double real = .0, double im = .0);
    friend Complex operator+(const Complex& one, const Complex& another);
    friend Complex operator-(const Complex& one, const Complex& another);
    friend Complex operator*(const Complex& one, const Complex& another);
    friend Complex operator/(const Complex& one, const Complex& another);
    friend bool operator==(const Complex& one, const Complex& another);
    Complex operator~() const;
    friend std::ostream& operator<<(std::ostream& os, const Complex& obj);
    double abs() const;
    double getReal() const;
};

#endif