#include <gtest/gtest.h>

#include "complex.h"

class ComplexFixture : public ::testing::Test {
    protected:
    Complex *oneNumber;
    Complex *anotherNumber;
    
    void SetUp() {
		oneNumber = new Complex(1., -1.);
		anotherNumber = new Complex(1., 1.);
	}

	void TearDown() {
		delete oneNumber;
        delete anotherNumber;
	}
};

TEST_F(ComplexFixture, mul) {
  ASSERT_EQ((*oneNumber) * (*anotherNumber), Complex(2, 0));
}

TEST_F(ComplexFixture, plus) {
  ASSERT_EQ((*oneNumber) + (*anotherNumber), Complex(2, 0));
}

TEST_F(ComplexFixture, minus) {
  ASSERT_EQ((*oneNumber) - (*anotherNumber), Complex(0, -2));
}

TEST_F(ComplexFixture, div) {
  ASSERT_EQ((*oneNumber) / Complex(.5, .0), Complex(2, -2));
}

TEST_F(ComplexFixture, conj) {
  ASSERT_EQ(~(*oneNumber), (*anotherNumber));
}

TEST_F(ComplexFixture, abs) {
  ASSERT_DOUBLE_EQ(pow(oneNumber->abs(), 2), ((*oneNumber) * ~(*oneNumber)).getReal());
}