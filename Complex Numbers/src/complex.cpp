#include "complex.h"

Complex::Complex(double real, double im): real(real), im(im) {};

Complex operator+(const Complex& one, const Complex& another) {
    return Complex(one.real + another.real, one.im + another.im);
};

Complex operator-(const Complex& one, const Complex& another) {
    return Complex(one.real - another.real, one.im - another.im);
};

Complex operator*(const Complex& one, const Complex& another) {
    double res_real = one.real * another.real - one.im * another.im;
    double res_im = one.real * another.im + one.im * another.real;
    return Complex(res_real, res_im);
};

Complex operator/(const Complex& one, const Complex& another) {
    double res_real = one.real * another.real + one.im * another.im;
    double res_im = one.im * another.real - another.real * another.im;
    double denom = pow(another.real, 2) + pow(another.im, 2); 
    return Complex(res_real / denom, res_im / denom);
};

bool operator==(const Complex& one, const Complex& another) {
    return (one.real == another.real) && (one.im == another.im);
};

Complex Complex::operator~() const {
    return Complex(this->real, - this->im);
};

std::ostream& operator<<(std::ostream& os, const Complex& obj) {
    if (obj.im > 0.) {
        os << obj.real << " + " << obj.im << "j";
    } else {
        os << obj.real << " - " << -obj.im << "j";
    }
    return os;
};

double Complex::abs() const {
    return sqrt(pow(this->real, 2) + pow(this->im, 2));
};

double Complex::getReal() const {
    return this->real;
};