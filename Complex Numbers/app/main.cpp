#include <assert.h>
#include <cmath>

#include "exampleConfig.h"
#include "complex.h"

int main() {
  std::cout << "Lib for complex numbers v"
            << PROJECT_VERSION_MAJOR
            << "."
            << PROJECT_VERSION_MINOR
            << std::endl;
  return 0;
}
