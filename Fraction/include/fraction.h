#ifndef FRACTION_H
#define FRACTION_H

#include <iostream>
#include <cstddef>

template <class T>
class Fraction {
    private:
    T num;
    T den;

    T gcd(T, T) const;
    T lcm(T, T) const;

    public:
    Fraction(T, T);
    Fraction(const Fraction<T>&);
    ~Fraction();

    Fraction<T> operator+(const Fraction<T>&);
    Fraction<T> operator-(const Fraction<T>&);
    Fraction<T> operator*(const Fraction<T>&);
    Fraction<T> operator/(const Fraction<T>&);
    Fraction<T>& operator=(const Fraction<T>&);
    bool operator==(const Fraction<T>&);
    bool operator<(const Fraction<T>&);
    bool operator>(const Fraction<T>&);

    template <class _T> 
    friend std::ostream& operator<<(std::ostream&, Fraction<_T>&);
};

#include "fraction.hpp"
#endif