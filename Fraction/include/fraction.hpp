#include "fraction.h"
#include <algorithm>
#include <stdexcept>

template <class T>
Fraction<T>::Fraction(T n, T d) : num(n), den(d) {
    if (d == 0) {
        throw std::overflow_error("Divide by zero exception");
    }
}

template <class T>
Fraction<T>::Fraction(const Fraction<T>& another) : num(another.num), den(another.den) {}

template <class T>
Fraction<T>::~Fraction() {}

template <class T>
T Fraction<T>::gcd(T a, T b) const {
    while (b) {
        a %= b;
        std::swap(a, b);
    }
    return a;
}

template <class T>
T Fraction<T>::lcm(T a, T b) const {
    return a*b / gcd(a, b);
}

template <class T>
Fraction<T> Fraction<T>::operator+(const Fraction<T>& another) {
    T newDen = lcm(den, another.den);
    T c1 = newDen / den;
    T c2 = newDen / another.den;
    T newNum = c1*num + c2*another.num;
    return Fraction<T>(newNum, newDen);
}

template <class T>
Fraction<T> Fraction<T>::operator-(const Fraction<T>& another) {
    T newDen = lcm(den, another.den);
    T c1 = newDen / den;
    T c2 = newDen / another.den;
    T newNum = c1*num - c2*another.num;
    return Fraction<T>(newNum, newDen);
}

template <class T>
Fraction<T> Fraction<T>::operator*(const Fraction<T>& another) {
    T newDen = den * another.den;
    T newNum = num * another.num;
    T gcd = this->gcd(newDen, newNum);
    return Fraction<T>(newNum / gcd, newDen / gcd);
}

template <class T>
Fraction<T> Fraction<T>::operator/(const Fraction<T>& another) {
    if (another.num == 0) {
        throw std::overflow_error("Divide by zero exception");
    }
    T newDen = den * another.num;
    T newNum = num * another.den;
    T gcd = this->gcd(newDen, newNum);
    return Fraction<T>(newNum / gcd, newDen / gcd);
}

template <class T>
Fraction<T>& Fraction<T>::operator=(const Fraction<T>& another) {
    if (this != &another) {
        this->num = another.num;
        this->den = another.den;
    }
    return *this;
}

template <class T>
bool Fraction<T>::operator==(const Fraction<T>& another) {
    T lcm = this->lcm(den, another.den);
    T c1 = lcm / den * num;
    T c2 = lcm / another.den * another.num;
    return c1 == c2;
}

template <class T>
bool Fraction<T>::operator<(const Fraction<T>& another) {
    T lcm = this->lcm(den, another.den);
    T c1 = lcm / den * num;
    T c2 = lcm / another.den * another.num;
    return c1 < c2;
}

template <class T>
bool Fraction<T>::operator>(const Fraction<T>& another) {
    T lcm = this->lcm(den, another.den);
    T c1 = lcm / den * num;
    T c2 = lcm / another.den * another.num;
    return c1 > c2;
}

template <class T>
std::ostream& operator<<(std::ostream& stream, Fraction<T>& fraction) {
    return stream << "Fraction(" << fraction.num << ", " << fraction.den << ")";
}