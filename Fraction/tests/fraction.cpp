#include <gtest/gtest.h>
#include <stdexcept>
#include "fraction.h"

TEST(Fraction, plus) {
    Fraction<int> f1(1, 2);
    Fraction<int> f2(1, 2);
    ASSERT_TRUE(f1 + f2 == Fraction<int>(1, 1));
}

TEST(Fraction, minus) {
    Fraction<int> f1(1, 3);
    Fraction<int> f2(1, 4);
    ASSERT_TRUE(f1 - f2 == Fraction<int>(1, 12));
}

TEST(Fraction, mul) {
    Fraction<int> f1(1, 3);
    Fraction<int> f2(1, 4);
    ASSERT_TRUE(f1 * f2 == Fraction<int>(1, 12));
}

TEST(Fraction, div) {
    Fraction<int> f1(1, 3);
    Fraction<int> f2(1, 4);
    ASSERT_TRUE(f1 / f2 == Fraction<int>(4, 3));
}

TEST(Fraction, invalid_div) {
    Fraction<int> f1(1, 3);
    Fraction<int> f2(0, 4);
    ASSERT_THROW(f1 / f2, std::overflow_error);
}

TEST(Fraction, comp) {
    Fraction<int> f1(1, 6);
    Fraction<int> f2(1, 8);
    ASSERT_TRUE(f1 > f2);
    ASSERT_TRUE(f2 < f1);
    ASSERT_TRUE(Fraction<int>(1, 1) == Fraction<int>(2, 2));
}

TEST(Fraction, assign) {
    Fraction<int> f1(1, 6);
    Fraction<int> f2(1, 5);
    f2 = f1;
    ASSERT_TRUE(f2 == Fraction<int>(1, 6));
}