#include <assert.h>
#include <iostream>

#include "exampleConfig.h"
#include "fraction.h"

int main() {
  std::cout << "My fraction v"
            << PROJECT_VERSION_MAJOR
            << "."
            << PROJECT_VERSION_MINOR
            << std::endl;
  return 0;
}
