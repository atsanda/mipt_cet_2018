#include <gtest/gtest.h>

#include "mystring.h"
#include <stdexcept>
#include <iostream>

TEST(MyString, eq) {
    MyString s1("one");
    MyString s2("oneanother");
    MyString s3(s1);
    ASSERT_FALSE(s1 == s2);
    ASSERT_TRUE(s1 == s3);
}

TEST(MyString, charat) {
    MyString s1("one");
    ASSERT_TRUE(s1[0] == 'o');
    ASSERT_THROW(s1[10], std::out_of_range);
}

TEST(MyString, concatenation) {
    MyString s1("one");
    MyString s2("two");
    MyString s3 = s1 + s2;
    ASSERT_TRUE(s3 == MyString("onetwo"));
    s1[0] = 'W';
    ASSERT_TRUE(s3 == MyString("onetwo"));
}

TEST(MyString, pluseq) {
    MyString s1("one");
    MyString s2("two");
    s1 += s2;
    ASSERT_TRUE(s1 == MyString("onetwo"));
    s2[0] = 'W';
    ASSERT_TRUE(s1 == MyString("onetwo"));
}

TEST(MyString, assign) {
    MyString s1("one");
    MyString s2;
    s2 = s1;
    ASSERT_FALSE(&s1 == &s2);
    ASSERT_TRUE(s1 == s2);
}

TEST(MyString, constructor) {
    MyString s1;
    ASSERT_TRUE(s1 == MyString(""));

    MyString s2("somestring", 4);
    ASSERT_TRUE(s2 == MyString("some"));
}

TEST(MyString, find) {
    MyString s2 = "aabaabaabaaab";
    ASSERT_TRUE(s2.find(MyString("aaab")) == (unsigned) 9);
    ASSERT_TRUE(s2.find(MyString("aab")) == (unsigned) 0);
    ASSERT_TRUE(s2.find(MyString("aab"), 1) == (unsigned) 3);
    ASSERT_TRUE((int) s2.find(MyString("abracadabrabrabra")) == (unsigned) -1);
    ASSERT_TRUE((int) s2.find(MyString("W")) == (unsigned) -1);
}
