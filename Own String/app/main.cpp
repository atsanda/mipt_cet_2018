#include <assert.h>
#include <iostream>

#include "exampleConfig.h"
#include "mystring.h"

int main() {
  std::cout << "My string implementation v"
            << PROJECT_VERSION_MAJOR
            << "."
            << PROJECT_VERSION_MINOR
            << std::endl;
  MyString s("Sintingring");
  std::cout << s << std::endl;
  MyString s2 = s;
  s[0] = 'L';
  std::cout << s << std::endl;
  std::cout << s2 << std::endl;
  s += s2;
  std::cout << s << std::endl;
  s[2] = 'W';
  std::cout << s + s2 << std::endl;
  std::cout << s2.find(MyString("ing")) << std::endl;
  return 0;
}
