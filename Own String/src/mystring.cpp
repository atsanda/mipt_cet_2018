#include "mystring.h"
#include <cstring>
#include <stdexcept>

MyString::MyString() {
    lenght = 0;
    characters = new char[lenght+1];
    characters[lenght] = '\0';
}

MyString::MyString(const char* s) {
    for (lenght = 0; s[lenght] != '\0'; lenght++);
    characters = new char[lenght+1];
    std::memcpy(characters, s, lenght+1);
}

MyString::MyString(const char* s, size_t lenght): lenght(lenght) {
    size_t s_lenght;
    for (s_lenght = 0; s[s_lenght] != '\0' && s_lenght <= lenght; s_lenght++);
    characters = new char[lenght+1];
    std::memcpy(characters, s, s_lenght+1);
}

MyString::MyString(const MyString& another) {
    lenght = another.size();
    characters = new char[lenght+1];
    std::memcpy(characters, another.characters, lenght+1);
}

std::size_t MyString::size() const {
    return lenght;
}

std::size_t MyString::find(const MyString& str, size_t pos) {
    size_t indx = pos;
    size_t sub_str_size = str.size();
    if (sub_str_size > lenght) {
        return -1;
    }   
    for (; indx < lenght-sub_str_size+1; indx++) {
        size_t i = 0;
        for (; i < sub_str_size && characters[indx+i] == str[i]; i++);
        if (i == sub_str_size) {
            break;
        }
    }
    if (indx == lenght-sub_str_size+1) {
        return -1;
    } else {
        return indx;
    }
}

const char& MyString::operator[](size_t pos) const {
    if (pos > lenght) {
        throw std::out_of_range("Index is out of bounds");
    }
    return characters[pos];
}

char& MyString::operator[](size_t pos) {
    if (pos > lenght) {
        throw std::out_of_range("Index is out of bounds");
    }
    return characters[pos];
}

MyString& MyString::operator+=(const MyString& s)  {
    size_t new_len = lenght + s.size();
    char* new_characters = new char[new_len+1];
    std::memcpy(new_characters, characters, lenght);
    std::memcpy(&new_characters[lenght], s.characters, s.lenght+1);
    delete[] characters;
    characters = new_characters;
    lenght = new_len;
    return *this;
}

MyString& MyString::operator=(const MyString& s) {
    if (this != &s) {
        delete[] characters;
        lenght = s.size();
        characters = new char[lenght+1];
        std::memcpy(characters, s.characters, lenght+1);
    }
    return *this;
}

MyString MyString::operator+(const MyString& s) {
    MyString this_copy(*this);
    this_copy += s;
    return this_copy;
}

bool MyString::operator==(const MyString& s) {
    if (lenght != s.size()) {
        return false;
    }
    return this->find(s) == 0;
}

std::ostream& operator<<(std::ostream& os, const MyString& obj) {
    for (std::size_t i=0; i < obj.lenght; i++) {
        os << obj[i];
    }
    return os;
}

MyString::~MyString() {
    delete[] characters;
}
