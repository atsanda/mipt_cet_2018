#ifndef MYSTRING_H
#define MYSTRING_H

#include <iostream>
#include <cstddef>

class MyString {
    private:
    
    char* characters;
    std::size_t lenght;

    public:

    MyString();
    MyString(const char* s);
    MyString(const char* s, size_t lenght);
    MyString(const MyString& another);
    std::size_t size() const;
    std::size_t find(const MyString& str, size_t pos=0);
    const char& operator[](size_t pos) const;
    char& operator[](size_t pos);
    MyString& operator+=(const MyString& s);
    MyString& operator=(const MyString& s);
    MyString operator+(const MyString& s);
    bool operator==(const MyString& s);
    friend std::ostream& operator<<(std::ostream& os, const MyString& obj);
    ~MyString();
};

#endif