#include <assert.h>
#include <cmath>
#include <iostream>

#include "exampleConfig.h"
#include "btree.h"

int main() {
  std::cout << "Lib for binary tree v"
            << PROJECT_VERSION_MAJOR
            << "."
            << PROJECT_VERSION_MINOR
            << std::endl;
  return 0;
}
