#include <gtest/gtest.h>
#include <set>
#include "btree.h"
#include <iostream>

TEST(BinaryTree, iter) {
    BinaryTree<int> tree;
    tree.insert(5);
    tree.insert(3);
    tree.insert(7);
    std::set<int> trueSet = {3, 5, 7};
    std::set<int> res;
    for(int n : tree) {
        res.insert(n);
    }
    ASSERT_TRUE(res == trueSet);
}

TEST(BinaryTree, depth) {
    BinaryTree<int> tree;
    ASSERT_TRUE(tree.maxDepth()  == 0);
    tree.insert(5);
    ASSERT_TRUE(tree.maxDepth()  == 1);
    tree.insert(4);
    ASSERT_TRUE(tree.maxDepth()  == 2);
    tree.insert(7);
    ASSERT_TRUE(tree.maxDepth()  == 2);
    tree.insert(8);
    ASSERT_TRUE(tree.maxDepth()  == 3);
}

TEST(BinaryTree, find) {
    BinaryTree<int> tree;
    tree.insert(5);
    tree.insert(4);
    tree.insert(7);
    tree.insert(8);

    ASSERT_TRUE(tree.find(10) == tree.end());
    BinaryTree<int>::iterator it_b = tree.find(7);
    std::set<int> trueSet = {7, 8};
    std::set<int> res;
    for(it_b; it_b != tree.end(); ++it_b) {
        res.insert(*it_b);
    }
    ASSERT_TRUE(res == trueSet);
}

TEST(BinaryTree, erase) {
    BinaryTree<int> tree;
    tree.insert(5);
    tree.insert(4);
    tree.insert(7);
    tree.insert(8);

    BinaryTree<int>::iterator it_b = tree.begin();
    it_b = tree.erase(it_b);

    std::set<int> trueSet = {4, 7, 8};
    std::set<int> res;
    for(it_b; it_b != tree.end(); ++it_b) {
        res.insert(*it_b);
    }
    ASSERT_TRUE(res == trueSet);

    it_b = tree.erase(tree.find(4));
    trueSet = {7, 8};
    res.clear();
    for(int n : tree) {
        res.insert(n);
    }
    ASSERT_TRUE(res == trueSet);
    ASSERT_TRUE(tree.maxDepth()  == 2);
    
    tree.insert(5);
    tree.insert(4);
    it_b = tree.erase(tree.find(5));
    trueSet = {4, 7, 8};
    res.clear();
    for(int n : tree) {
        res.insert(n);
    }
    ASSERT_TRUE(res == trueSet);
    ASSERT_TRUE(tree.maxDepth()  == 2);

    tree.insert(3);
    tree.insert(5);
    tree.insert(6);
    it_b = tree.erase(tree.find(5));
    trueSet = {3, 4, 6, 7, 8};
    res.clear();
    for(int n : tree) {
        res.insert(n);
    }
    ASSERT_TRUE(res == trueSet);
    ASSERT_TRUE(tree.maxDepth()  == 3);
}