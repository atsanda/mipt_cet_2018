#ifndef BTREE_H
#define BTREE_H

#include <functional>
#include <cstddef>
#include <list>


template <class T>
class Node {
    private:
    T data;
    Node<T>* left;
    Node<T>* right;

    template <class _T, class Compare>
    friend class BinaryTree;
    
    public:
    Node(T data) : data(data), left(NULL), right(NULL) {};
    bool isLeaf();
};


template <class T, class Compare = std::less<T>>
class BinaryTree {
    private:
    Node<T>* root;
    Compare cmp;
    bool equal(T& one, T& another);
    
    public:
    class iterator : std::iterator<std::input_iterator_tag, T> { 
        private:
        std::list<Node<T>*>* remained;
        Node<T>* current;

        template <class _T, class _Compare>
        friend class BinaryTree;

        public:
        iterator(Node<T>*);
        iterator();
        iterator(const iterator&);
        ~iterator();

        iterator& operator=(const iterator&);
        bool operator==(const iterator&) const;
        bool operator!=(const iterator&) const;
        iterator& operator++();

        T& operator*() const;
        T* operator->() const;
    };

    BinaryTree(const Compare& c = Compare()): root(NULL), cmp(c) {};
    ~BinaryTree() {};
    void insert(T data);
    iterator find(T data);
    iterator erase(iterator pos);
    std::size_t maxDepth();
    iterator begin();
    iterator end();
};


#include "btree.hpp"
#endif