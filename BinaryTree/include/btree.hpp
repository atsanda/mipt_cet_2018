// btree.hpp
#include <list>
#include <iostream>


// Node methods
template <class T>
bool Node<T>::isLeaf() {
    return !left && !right; 
}


// BinaryTree methods
template <class T, class Compare>
void BinaryTree<T, Compare>::insert(T data) {
    Node<T>* newNode = new Node<T>(data);
    if (root == NULL) {
        root = newNode;
        return;
    }

    Node<T>* iter = root;
    while(true) {
        if (cmp(data, iter->data)) {
            if (iter->left != NULL) {
                iter = iter->left;
                continue;
            } else {
                iter->left = newNode;
                return;
            }
        } else {
            if (iter->right != NULL) {
                iter = iter->right;
                continue;
            } else {
                iter->right = newNode;
                return;
            }
        }
    }
}


template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator BinaryTree<T, Compare>::find(T data) {
    if (root == NULL) {
        return this->end();
    }

    Node<T>* iter = root;
    while(iter != NULL) {
        if (equal(iter->data, data)) {
            return iterator(iter);
        }

        if (cmp(data, iter->data)) {
            iter = iter->left;
        } else {
            iter = iter->right;
        }
    }
    return this->end();
}


template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator BinaryTree<T, Compare>::erase(typename BinaryTree<T, Compare>::iterator pos) {
    if (root == NULL) {
        return this->end();
    }
    // searching parent
    Node<T>* iter = root;
    Node<T>** to_paste;
    bool found = false;
    if (pos.current == root) {
        found = true;
        to_paste = &root;
    }
    while(iter != NULL && !found) {
        if (iter->left != NULL && pos.current == iter->left) {
            found = true;
            to_paste = &iter->left;
            break;
        }
        if (iter->right != NULL && pos.current == iter->right) {
            found = true;
            to_paste = &iter->right;
            break;
        }

        if (cmp(*pos, iter->data)) {
            iter = iter->left;
        } else {
            iter = iter->right;
        }
    }
    
    if (!found) {
        return this->end();
    }
    // erasing
    if (pos.current->left == NULL && pos.current->right == NULL) {
        *to_paste = NULL;
        delete pos.current;
        return this->end();
    } else if (pos.current->left != NULL && pos.current->right == NULL) {
        *to_paste = pos.current->left;
        delete pos.current;
        return ++pos;
    } else if (pos.current->left == NULL && pos.current->right != NULL) {
        *to_paste = pos.current->right;
        delete pos.current;
        return ++pos;
    } else {
        Node<T>* most_left = pos.current->right;
        while (most_left->left != NULL) {most_left = most_left->left;}
        most_left->left = pos.current->left;
        *to_paste = pos.current->right;
        delete pos.current;
        pos.remained->remove(pos.current->left);
        return ++pos;
    }
}


template <class T, class Compare>
std::size_t BinaryTree<T, Compare>::maxDepth() {
    if (root == NULL) {
        return 0;
    }
    std::size_t maxDepth = 1;
    std::list<Node<T>*> cur_lvl, next_lvl;
    cur_lvl.push_back(root);
    if (root->left != NULL) {
        next_lvl.push_back(root->left);
    }
    if (root->right != NULL) {
        next_lvl.push_back(root->right);
    }
    while (!next_lvl.empty()) {
        maxDepth++;
        cur_lvl = next_lvl;
        next_lvl.clear();
        for (Node<T>* n: cur_lvl) {
            if (n->left != NULL) {
                next_lvl.push_back(n->left);
            }
            if (n->right != NULL) {
                next_lvl.push_back(n->right);
            }
        }
    }
    return maxDepth;
}


// iterator methods
template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator BinaryTree<T, Compare>::begin() {
    return iterator(root);
}

template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator BinaryTree<T, Compare>::end() {
    return iterator();
}

template <class T, class Compare>
BinaryTree<T, Compare>::iterator::iterator(Node<T>* beginNode) {
    current = beginNode;
    remained = new std::list<Node<T>*>();
    if (beginNode->left != NULL) {
        remained->push_back(beginNode->left);
    } 
    if (beginNode->right != NULL) {
        remained->push_back(beginNode->right);
    }
}

template <class T, class Compare>
BinaryTree<T, Compare>::iterator::iterator() {
    remained = new std::list<Node<T>*>();
    current = NULL;
}

template <class T, class Compare>
BinaryTree<T, Compare>::iterator::~iterator() {
    delete remained;
}

template <class T, class Compare>
BinaryTree<T, Compare>::iterator::iterator(const iterator& iter) {
    remained = new std::list<Node<T>*>(*iter.remained);
    current = iter.current;
}

template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator& BinaryTree<T, Compare>::iterator::operator=(const iterator& iter) {
    if (this != &iter) {
        delete remained;
        remained = new std::list<Node<T>*>(*iter.remained);
        current = iter.current;
    }
    return *this;
}

template <class T, class Compare>
bool BinaryTree<T, Compare>::iterator::operator==(const iterator& iter) const {
    return current == iter.current && *remained == *(iter.remained);
}

template <class T, class Compare>
bool BinaryTree<T, Compare>::iterator::operator!=(const iterator& iter) const {
    return !this->operator==(iter);
}

template <class T, class Compare>
typename BinaryTree<T, Compare>::iterator& BinaryTree<T, Compare>::iterator::operator++() {
    current = NULL;
    if (!remained->empty()){
        current = remained->back();
        remained->pop_back();
        if (current->left != NULL) {
            remained->push_back(current->left);
        } 
        if (current->right != NULL) {
            remained->push_back(current->right);
        }
    }
    return *this;
}

template <class T, class Compare>
T& BinaryTree<T, Compare>::iterator::operator*() const {
    return current->data;
}

template <class T, class Compare>
T* BinaryTree<T, Compare>::iterator::operator->() const {
    return &current->data;
}

template <class T, class Compare>
bool BinaryTree<T, Compare>::equal(T& one, T& another) {
    return !cmp(one, another) && !cmp(another, one);
}